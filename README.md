# Scala Web Crwaling Tool

## 설명

Web Crawling 도구를 만들어서 Web의 내용을 긁어오고, 메일 전송 및 Telegram연동을 통해 특정 조건의 데이터가 왔을 때 알려준다.

메일 전송시 인터넷이 안되는 환경에서 Web의 내용을 조회할 수 있게 한다. (이미지 등 표시 필요함)

## 기술 스택

Scala + Play + Akka
MongoDB(이게 필요할까? )
